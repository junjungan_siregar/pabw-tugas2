<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class Home extends CI_Controller {
	public function index(){
        $data['title'] = 'Halaman Home';
        $data['page'] = 'lorem';
        $this->load->view('admin/overview', $data);
	}
}